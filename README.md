# Reality Rook
This project was created to solve a single problem: coordinating sessions between 15 players for different seperately running Table Top Campaigns based on who can make when. Our shared Discord server quickly became a time management nightmare, leading to the development of Reality Rook in a weekend to act as an automated schedule checker to optimize game running.

## Goal
The end goal of Reality Rook is to be a self-hosted one-stop solution for groups of folks with many Table Top Campaigns between them, looking for coordination. Reality Rook should be hosted with one instance per group, with integrations around a core web interface and database.

## Current State
Nobody should be using Reality Rook. Currently it was scrapped together in a weekend with quick hacky solutions and libraries to get core functionality out quick.